import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { SharedModule } from '@shared/shared.module';

import { AppComponent } from '@app/app.component';
import { AppRoutingModule } from '@app/app-routing.module';

import { SidenavComponent } from '@app/layout/sidenav/sidenav.component';
import { ContentLayoutComponent } from '@app/layout/content-layout/content-layout.component';
import { ToolbarComponent } from '@app/layout/toolbar/toolbar.component';

@NgModule({
  declarations: [
    AppComponent,
    ContentLayoutComponent,
    ToolbarComponent,
    SidenavComponent,
  ],
  imports: [
    BrowserModule,
    SharedModule,
    AppRoutingModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
