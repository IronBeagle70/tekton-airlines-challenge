import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';

import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';

import { SidenavComponent } from '@layout/sidenav/sidenav.component';
import { ToolbarComponent } from '@layout/toolbar/toolbar.component';
import { ContentLayoutComponent } from '@layout/content-layout/content-layout.component';

describe('ContentLayoutComponent', () => {
  let component: ContentLayoutComponent;
  let fixture: ComponentFixture<ContentLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatSidenavModule,
        BrowserAnimationsModule,
        MatDividerModule,
        MatToolbarModule,
        MatIconModule,
      ],
      declarations: [
        ContentLayoutComponent,
        SidenavComponent,
        ToolbarComponent,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ContentLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('content layout should be created', () => {
    expect(component).toBeTruthy();
  });
  it('sidenav component should be created', () => {
    const content =
      fixture.debugElement.nativeElement.querySelectorAll('app-sidenav');

    expect(content[0]).toBeTruthy();
  });
});
