import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';

import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenav } from '@angular/material/sidenav';

import { ToolbarComponent } from './toolbar.component';

describe('ToolbarComponent', () => {
  let component: ToolbarComponent;
  let fixture: ComponentFixture<ToolbarComponent>;
  let el: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MatToolbarModule, MatIconModule],
      declarations: [ToolbarComponent],
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(ToolbarComponent);
        component = fixture.componentInstance;
        el = fixture.debugElement;
      });
  });

  it('toolbar should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should display the title', () => {
    const sidenav = jasmine.createSpyObj<MatSidenav>('MatSidenav', ['toggle']);
    component.sidenav = sidenav;
    component.title = 'Tekton Airlines';
    fixture.detectChanges();

    const title = fixture.nativeElement.querySelector('.menu');

    expect(title.textContent.trim()).toEqual('Tekton Airlines');
  });

  it('should toggle the sidenav when the button is clicked', fakeAsync(() => {
    const sidenav = jasmine.createSpyObj<MatSidenav>('MatSidenav', ['toggle']);

    component.sidenav = sidenav;  
    sidenav.mode = 'over';
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      const button = el.nativeElement.querySelector('button');
      button.click();
      expect(sidenav.toggle).toHaveBeenCalled();
    });
  }));

  it('should display the menu icon when the sidenav is closed', () => {
    const sidenav = jasmine.createSpyObj<MatSidenav>('MatSidenav', [], {
      opened: false,
    });

    component.sidenav = sidenav;
    sidenav.mode = 'over';
    fixture.detectChanges();

    const menuIcon = el.nativeElement.querySelector('mat-icon');

    expect(menuIcon).toBeTruthy();
    expect(menuIcon.textContent.trim()).toEqual('menu');
  });

  it('should display the close icon when the sidenav is opened', () => {
    const sidenav = jasmine.createSpyObj<MatSidenav>('MatSidenav', [], {
      opened: true,
    });

    component.sidenav = sidenav;
    sidenav.mode = 'over';
    fixture.detectChanges();

    const closeIcon = el.nativeElement.querySelector('mat-icon');

    expect(closeIcon).toBeTruthy();
    expect(closeIcon.textContent.trim()).toEqual('close');
  });
});
