import { TestBed } from '@angular/core/testing';
import { FormControl } from '@angular/forms';

import { ValidatorsService } from './validators.service';

describe('ValidatorsService', () => {
  let service: ValidatorsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ValidatorsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  const testData = [
    {
      type: 'DNI',
      validator: 'dniMustBeValid',
      validValues: ['12345678'],
      invalidValues: ['', '12345', '12345abc', '123456789', 'abc'],
    },
    {
      type: 'CE',
      validator: 'ceMustBeValid',
      validValues: ['123456789', '123abc789'],
      invalidValues: ['', '12345', '12345abc', '12345678', '1234567890'],
    },
    {
      type: 'Pasaporte',
      validator: 'passportMustBevalid',
      validValues: ['123456789'],
      invalidValues: ['', '12345', 'abc', '123456abc', '1234567890'],
    },
  ];

  testData.forEach((data) => {
    it(`should set ${data.type} validators correctly`, () => {
      const control = new FormControl('');
      service.setupDocumentNumberValidators(control, data.type);

      data.invalidValues.forEach((value) => {
        control.setValue(value);
        expect(control.invalid).toBe(true);

        if (value === '') {
          expect(control.errors?.['required']).toBeTruthy();
        } else {
          expect(control.errors?.[data.validator]).toBeTruthy();
        }
      });

      data.validValues.forEach((value) => {
        control.setValue(value);
        expect(control.valid).toBe(true);
      });
    });
  });
});
