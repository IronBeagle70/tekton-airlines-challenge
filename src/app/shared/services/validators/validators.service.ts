import { Injectable } from '@angular/core';

import { FormControl } from '@angular/forms';

import { ceMustBeValid } from '@shared/custom-validators/document-types/ce.validator';
import { dniMustBeValid } from '@shared/custom-validators/document-types/dni.validator';
import { passportMustBevalid } from '@shared/custom-validators/document-types/pasaporte.validator';

@Injectable({
  providedIn: 'root'
})
export class ValidatorsService {

  setupDocumentNumberValidators(control: FormControl, documentType: string): void {
    control.clearValidators();

    switch (documentType) {
      case 'DNI':
        control.setValidators([dniMustBeValid]);
        break;
      case 'CE':
        control.setValidators([ceMustBeValid]);
        break;
      case 'Pasaporte':
        control.setValidators([passportMustBevalid]);
        break;
    }

    control.markAsDirty();
    control.updateValueAndValidity();
  }
}
