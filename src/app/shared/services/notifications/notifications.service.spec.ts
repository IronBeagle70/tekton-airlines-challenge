import { TestBed } from '@angular/core/testing';

import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

import { of } from 'rxjs';

import { NotificationsService } from './notifications.service';

import { ConfirmDialogComponent } from '@shared/components/confirm-dialog/confirm-dialog.component';
import { NotifierComponent } from '@shared/components/notifier/notifier.component';


describe('NotificationsService', () => {
  let service: NotificationsService;
  let matSnackBar: MatSnackBar;
  let matSnackBarSpy: jasmine.Spy;
  let matDialog: MatDialog;
  let matDialogSpy: jasmine.SpyObj<MatDialog>;

  beforeEach(() => {
    matSnackBarSpy = jasmine.createSpyObj('MatSnackBar', ['openFromComponent']);
    matDialogSpy = jasmine.createSpyObj('MatDialog', ['open']);
    TestBed.configureTestingModule({
      providers: [
        NotificationsService,
        {
          provide: MatSnackBar,
          useValue: matSnackBarSpy,
        },
        {
          provide: MatDialog,
          useValue: matDialogSpy,
        },
      ],
    });
    service = TestBed.inject(NotificationsService);
    matSnackBar = TestBed.inject(MatSnackBar);
    matDialog = TestBed.inject(MatDialog);
  });

  it('notifier service should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call openFromComponent method of MatSnackBar', () => {
    const displayMessage = 'Test message';
    const buttonText = 'Test button text';
    service.showNotification(displayMessage, buttonText);
    expect(matSnackBar.openFromComponent).toHaveBeenCalledWith(
      NotifierComponent,
      jasmine.objectContaining({
        duration: 5000,
        data: { message: displayMessage, buttonText: buttonText },
        horizontalPosition: 'center',
        verticalPosition: 'bottom',
      })
    );
  });

  it('should create a confirm dialog', () => {
    const titleMessage = 'Test title dialog';
    const displayMessage = 'Test message dialog';

    const dialogRefSpyObj = jasmine.createSpyObj({ afterClosed : of({}) });
    matDialogSpy.open.and.returnValue(dialogRefSpyObj);
    
    service.showConfirmDialog(titleMessage, displayMessage);
    expect(matDialog.open).toHaveBeenCalledWith(
      ConfirmDialogComponent,
      jasmine.objectContaining({
        data: { title: titleMessage, message: displayMessage },
      })
    );
    expect(dialogRefSpyObj.afterClosed).toHaveBeenCalled();
  });
});
