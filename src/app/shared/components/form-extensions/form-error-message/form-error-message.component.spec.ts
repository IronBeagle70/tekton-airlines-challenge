import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormControl, Validators } from '@angular/forms';

import { By } from '@angular/platform-browser';

import { ceMustBeValid } from '@shared/custom-validators/document-types/ce.validator';
import { dniMustBeValid } from '@shared/custom-validators/document-types/dni.validator';
import { passportMustBevalid } from '@shared/custom-validators/document-types/pasaporte.validator';
import { namesMustbeValid } from '@shared/custom-validators/names.validator';
import { nationalityMustbeValid } from '@shared/custom-validators/nationality.validator';
import { surnamesMustbeValid } from '@shared/custom-validators/surnames.validator';

import { FormErrorContainerComponent } from '../form-error-container/form-error-container.component';
import { FormErrorMessageComponent } from './form-error-message.component';

describe('FormErrorMessageComponent', () => {
  let component: FormErrorMessageComponent;
  let fixture: ComponentFixture<FormErrorMessageComponent>;
  let el: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FormErrorMessageComponent],
      providers: [FormErrorContainerComponent],
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(FormErrorMessageComponent);
        const parentComponent = fixture.debugElement.componentInstance[
          'errorContainer'
        ] as FormErrorContainerComponent;
        component = fixture.componentInstance;
        component.control = parentComponent.control;
        el = fixture.debugElement;
      });
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should display the required error message', () => {
    const control = new FormControl('', [Validators.required]);
    component.control = control;

    component.errorCode = 'required';

    const spy = spyOn(component.control, 'hasError').and.callThrough();
    component.control.hasError(component.errorCode);

    fixture.detectChanges();

    const errorMessage = el.query(By.css('p'));
    expect(errorMessage).toBeDefined();

    expect(spy).toHaveBeenCalledWith('required');
    expect(component.errorMsg).toEqual('*El campo es obligatorio.');
  });

  it('should display the namesMustbeValid error message', () => {
    const control = new FormControl('3', [namesMustbeValid]);
    component.control = control;

    component.errorCode = 'namesMustbeValid';

    const spy = spyOn(component.control, 'hasError').and.callThrough();
    component.control.hasError(component.errorCode);

    fixture.detectChanges();

    const errorMessage = el.query(By.css('p'));
    expect(errorMessage).toBeDefined();

    expect(spy).toHaveBeenCalledWith('namesMustbeValid');
    expect(component.errorMsg).toEqual(
      '*Use caracteres alfabéticos, y especiales (acentos, ñ, ‘,.)'
    );
  });

  it('should display the surnamesMustbeValid error message', () => {
    const control = new FormControl('3', [surnamesMustbeValid]);
    component.control = control;

    component.errorCode = 'surnamesMustbeValid';

    const spy = spyOn(component.control, 'hasError').and.callThrough();
    component.control.hasError(component.errorCode);

    fixture.detectChanges();

    const errorMessage = el.query(By.css('p'));
    expect(errorMessage).toBeDefined();

    expect(spy).toHaveBeenCalledWith('surnamesMustbeValid');
    expect(component.errorMsg).toEqual(
      '*Use caracteres alfabéticos, y especiales (acentos, ñ, ‘,.)'
    );
  });

  it('should display the nationalityMustbeValid error message', () => {
    const control = new FormControl('3', [nationalityMustbeValid]);
    component.control = control;

    component.errorCode = 'nationalityMustbeValid';

    const spy = spyOn(component.control, 'hasError').and.callThrough();
    component.control.hasError(component.errorCode);

    fixture.detectChanges();

    const errorMessage = el.query(By.css('p'));
    expect(errorMessage).toBeDefined();

    expect(spy).toHaveBeenCalledWith('nationalityMustbeValid');
    expect(component.errorMsg).toEqual('*Use solo caracteres alfabéticos');
  });

  it('should display the dniMustbeValid error message', () => {
    const control = new FormControl('3', [dniMustBeValid]);
    component.control = control;

    component.errorCode = 'dniMustBeValid';

    const spy = spyOn(component.control, 'hasError').and.callThrough();
    component.control.hasError(component.errorCode);

    fixture.detectChanges();

    const errorMessage = el.query(By.css('p'));
    expect(errorMessage).toBeDefined();

    expect(spy).toHaveBeenCalledWith('dniMustBeValid');
    expect(component.errorMsg).toEqual(
      '*DNI: Use solo caracteres númericos hasta 8 dígitos'
    );
  });

  it('should display the ceMustbeValid error message', () => {
    const control = new FormControl('3', [ceMustBeValid]);
    component.control = control;

    component.errorCode = 'ceMustBeValid';

    const spy = spyOn(component.control, 'hasError').and.callThrough();
    component.control.hasError(component.errorCode);

    fixture.detectChanges();

    const errorMessage = el.query(By.css('p'));
    expect(errorMessage).toBeDefined();

    expect(spy).toHaveBeenCalledWith('ceMustBeValid');
    expect(component.errorMsg).toEqual(
      '*CE: Use solo caracteres alfanúmericos hasta 9 dígitos'
    );
  });

  it('should display the passportMustbeValid error message', () => {
    const control = new FormControl('3', [passportMustBevalid]);
    component.control = control;

    component.errorCode = 'passportMustBevalid';

    const spy = spyOn(component.control, 'hasError').and.callThrough();
    component.control.hasError(component.errorCode);

    fixture.detectChanges();

    const errorMessage = el.query(By.css('p'));
    expect(errorMessage).toBeDefined();

    expect(spy).toHaveBeenCalledWith('passportMustBevalid');
    expect(component.errorMsg).toEqual(
      '*Pasaporte: Use solo caracteres númericos hasta 9 dígitos'
    );
  });
});
