import { Component, Input } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-form-error-container',
  templateUrl: './form-error-container.component.html',
  styleUrls: ['./form-error-container.component.scss']
})
export class FormErrorContainerComponent {
  // eslint-disable-next-line @angular-eslint/no-input-rename
  @Input('forControl') control!: AbstractControl | null;
}
