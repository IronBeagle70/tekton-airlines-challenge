import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  FormControl,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';

import { By } from '@angular/platform-browser';
import { SharedModule } from '@shared/shared.module';

import { FormErrorMessageComponent } from '../form-error-message/form-error-message.component';
import { FormErrorContainerComponent } from './form-error-container.component';

describe('FormErrorContainerComponent', () => {
  let component: FormErrorContainerComponent;
  let fixture: ComponentFixture<FormErrorContainerComponent>;
  let el: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FormErrorContainerComponent, FormErrorMessageComponent],
      imports: [FormsModule, ReactiveFormsModule, SharedModule],
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(FormErrorContainerComponent);
        component = fixture.componentInstance;
        el = fixture.debugElement;
      });
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should show content if control is invalid and dirty or touched', () => {
    const control = new FormControl('', Validators.required);
    component.control = control;
    control.markAsDirty();
    control.markAsTouched();

    fixture.detectChanges();

    expect(component.control.status).toBe('INVALID');
    const element = el.query(By.css('ng-container'));
    expect(element).toBeDefined();
  });

  it('should not show content if control is valid', () => {
    const control = new FormControl('some value', Validators.required);
    component.control = control;
    fixture.detectChanges();

    expect(component.control.status).toBe('VALID');
    const element = el.query(By.css('ng-container'));
    expect(element).toBeNull();
  });
});
