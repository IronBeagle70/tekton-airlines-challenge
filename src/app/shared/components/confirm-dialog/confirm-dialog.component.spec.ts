import { DebugElement } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import {
  ConfirmationData,
  ConfirmDialogComponent,
} from './confirm-dialog.component';

describe('ConfirmDialogComponent', () => {
  let component: ConfirmDialogComponent;
  let fixture: ComponentFixture<ConfirmDialogComponent>;
  let el: DebugElement;
  let dialogRefSpy: jasmine.SpyObj<MatDialogRef<ConfirmDialogComponent>>;

  beforeEach(async () => {
    dialogRefSpy = jasmine.createSpyObj('MatDialogRef', ['close']);
    await TestBed.configureTestingModule({
      declarations: [ConfirmDialogComponent],
      providers: [
        {
          provide: MAT_DIALOG_DATA,
          useValue: {},
        },
        {
          provide: MatDialogRef,
          useValue: dialogRefSpy,
        },
      ],
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(ConfirmDialogComponent);
        component = fixture.componentInstance;
        el = fixture.debugElement;
      });
  });

  it('dialog component should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should display the title and message text', fakeAsync(() => {
    const confirmationData: ConfirmationData = {
      title: 'Test title',
      message: 'Test message',
    };

    component.data = confirmationData;
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      const titleElement = fixture.nativeElement.querySelector('.title');
      expect(titleElement.textContent).toContain(confirmationData.title);

      const messageElement = el.nativeElement.querySelectorAll('div');
      const messageContainer = messageElement[0];
      expect(messageContainer.textContent).toContain(confirmationData.message);
    });
  }));

  it('should call the confirm function when the confirm button is clicked', () => {
    const spy = spyOn(component, 'onConfirm');

    const confirmationData: ConfirmationData = {
      title: 'Test title',
      message: 'Test message',
    };

    component.data = confirmationData;
    fixture.detectChanges();

    const buttons = el.nativeElement.querySelectorAll('button');
    const buttonConfirm = buttons[1];
    buttonConfirm.click();

    expect(spy).toHaveBeenCalled();
  });

  it('should call the cancel function when the cancel button is clicked', () => {
    const spy = spyOn(component, 'onCancel');

    const confirmationData: ConfirmationData = {
      title: 'Test title',
      message: 'Test message',
    };

    component.data = confirmationData;
    fixture.detectChanges();

    const buttons = el.nativeElement.querySelectorAll('button');
    const buttonCancel = buttons[0];
    buttonCancel.click();

    expect(spy).toHaveBeenCalled();
  });

  it('should close the dialog with true when the action is confirmed', () => {
    component.onConfirm();
    expect(dialogRefSpy.close).toHaveBeenCalledWith(true);
  });

  it('should close the dialog with false when the action is canceled', () => {
    component.onCancel();
    expect(dialogRefSpy.close).toHaveBeenCalledWith(false);
  });
});
