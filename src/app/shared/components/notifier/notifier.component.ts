import { Component, Inject } from '@angular/core';
import {
  MatSnackBarRef,
  MAT_SNACK_BAR_DATA,
} from '@angular/material/snack-bar';

export interface NotificationData {
  message: string;
  buttonText: string;
}

@Component({
  selector: 'app-notifier',
  templateUrl: './notifier.component.html',
  styleUrls: ['./notifier.component.scss'],
})
export class NotifierComponent {
  constructor(
    @Inject(MAT_SNACK_BAR_DATA)
    public data: NotificationData,
    public snackBarRef: MatSnackBarRef<NotifierComponent>
  ) {}
}
