import { TestBed } from '@angular/core/testing';
import { BtnMessagePipe } from './btn-message.pipe';

describe('BtnMessagePipe', () => {
  let pipe: BtnMessagePipe;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BtnMessagePipe],
    });
    pipe = TestBed.inject(BtnMessagePipe);
  });

  it('pipe btn message is created', () => {
    expect(pipe).toBeTruthy();
  });

  it('should transform BtnMessagePipe when passengersNumber is 0', () => {
    const transformedValue = pipe.transform(0);
    expect(transformedValue).toEqual('Registrar');
  });

  it('should transform BtnMessagePipe when passengersNumber is not 0', () => {
    const transformedValue = pipe.transform(4);
    expect(transformedValue).toEqual('Seguir Registrando');
  });
});

