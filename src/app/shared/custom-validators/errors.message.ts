interface ErrorsMessages {
  [key: string]: string;
}

export const errorsMessages: ErrorsMessages = {
  required: '*El campo es obligatorio.',

  namesMustbeValid: '*Use caracteres alfabéticos, y especiales (acentos, ñ, ‘,.)',
  surnamesMustbeValid: '*Use caracteres alfabéticos, y especiales (acentos, ñ, ‘,.)',
  nationalityMustbeValid: '*Use solo caracteres alfabéticos',

  dniMustBeValid: '*DNI: Use solo caracteres númericos hasta 8 dígitos',
  passportMustBevalid: '*Pasaporte: Use solo caracteres númericos hasta 9 dígitos',
  ceMustBeValid: '*CE: Use solo caracteres alfanúmericos hasta 9 dígitos'
};
