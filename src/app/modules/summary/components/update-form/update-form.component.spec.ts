import { DebugElement } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { NotificationsService } from '@shared/services/notifications/notifications.service';
import { UpdateService } from '@modules/summary/services/update-form/update.service';

import { FormErrorMessageComponent } from '@shared/components/form-extensions/form-error-message/form-error-message.component';
import { FormErrorContainerComponent } from '@shared/components/form-extensions/form-error-container/form-error-container.component';
import { UpdateFormComponent } from './update-form.component';

describe('UpdateFormComponent', () => {
  let component: UpdateFormComponent;
  let fixture: ComponentFixture<UpdateFormComponent>;
  let el: DebugElement;
  let notifierService: NotificationsService;
  let updateService: UpdateService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MatSnackBarModule, MatDialogModule, ReactiveFormsModule],
      declarations: [
        UpdateFormComponent,
        FormErrorContainerComponent,
        FormErrorMessageComponent,
      ],
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(UpdateFormComponent);
        component = fixture.componentInstance;
        el = fixture.debugElement;
        notifierService = TestBed.inject(NotificationsService);
        updateService = TestBed.inject(UpdateService);
      });
  });

  it('update form should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should display update form', () => {
    const updateContainer = el.nativeElement.querySelectorAll('.form');
    const updateInputContainer = el.nativeElement.querySelectorAll(
      '.form__container div'
    );
    const updateTitle = el.nativeElement.querySelectorAll('.form__title');

    expect(updateContainer).toBeTruthy();
    expect(updateTitle[0].innerText).toBe('Actualizar Pasajero');
    expect(updateInputContainer.length).toBe(6);
  });

  it('should initialize update form with passenger data', () => {
    component.passenger = {
      names: 'Names test',
      surnames: 'Surnames test',
      nationality: 'Nationality test',
      documentType: 'DNI',
      documentNumber: '12345678',
    };

    fixture.detectChanges();

    expect(component.newNamesControl.value).toEqual('Names test');
    expect(component.newSurnames.value).toEqual('Surnames test');
    expect(component.newNationality.value).toEqual('Nationality test');
    expect(component.newDocumentTypeControl.value).toEqual('DNI');
    expect(component.newDocumentNumberControl.value).toEqual('12345678');
  });

  it('should update the passenger data', fakeAsync(() => {
    const spy = spyOn(component, 'onUpdatePassenger').and.callThrough();
    const notifierSpy = spyOn(notifierService, 'showNotification');
    const updateServiceSpy = spyOn(updateService, 'setIsEditing');

    component.passenger = {
      names: 'Names test',
      surnames: 'Surnames test',
      nationality: 'Nationality test',
      documentType: 'DNI',
      documentNumber: '12345678',
    };

    fixture.detectChanges();

    component.updateForm.setValue({
      newNames: 'New names',
      newSurnames: 'New surnames',
      newNationality: component.passenger.names,
      newDocumentType: 'Passport',
      newDocumentNumber: '123456789',
    });

    const button = el.nativeElement.querySelector('button[type=submit]');

    button.click();
    fixture.detectChanges();
    tick();

    expect(spy).toHaveBeenCalled();
    expect(updateServiceSpy).toHaveBeenCalledWith(false);
    expect(notifierSpy).toHaveBeenCalledWith(
      'Pasajero Actualizado Satisfactoriamente',
      'Ok'
    );
  }));

  it('should cancel the passenger update form', () => {
    const spy = spyOn(component, 'onClose').and.callThrough();
    const updateServiceSpy = spyOn(updateService, 'setIsEditing');

    component.passenger = {
      names: 'Names test',
      surnames: 'Surnames test',
      nationality: 'Nationality test',
      documentType: 'DNI',
      documentNumber: '12345678',
    };

    const buttons = el.nativeElement.querySelectorAll('button');
    const cancelButton = buttons[1];

    cancelButton.click();
    fixture.detectChanges();

    expect(spy).toHaveBeenCalled();
    expect(updateServiceSpy).toHaveBeenCalledWith(false);
  });
});
