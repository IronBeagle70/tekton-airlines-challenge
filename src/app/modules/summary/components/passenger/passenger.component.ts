import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Passenger } from '@shared/models/passenger/passenger.model';

import { NotificationsService } from '@shared/services/notifications/notifications.service';
import { StoreService } from '@shared/services/store/store.service';
import { UpdateService } from '@modules/summary/services/update-form/update.service';

@Component({
  selector: 'app-passenger',
  templateUrl: './passenger.component.html',
  styleUrls: ['./passenger.component.scss'],
})
export class PassengerComponent implements OnInit {
  @Input() passenger!: Passenger;
  private subscription: Subscription = new Subscription();

  isEditing = false;

  constructor(
    private storeService: StoreService,
    private updateService: UpdateService,
    private notifierService: NotificationsService
  ) {}

  ngOnInit(): void {
    this.subscription = this.storeService.passengers$.subscribe();
    this.updateService.isEditing$.subscribe((value) => {
      this.isEditing = value;
    });
  }
  
  onDeletePassenger() {
    this.notifierService
      .showConfirmDialog(
        'Eliminar Pasajero',
        '¿Desea eliminar los datos de este pasajero?'
      )
      .subscribe((confirmed) => {
        if (confirmed) {
          const index = this.storeService
            .getPassengers()
            .indexOf(this.passenger);
          this.storeService.removePassenger(index);
          this.subscription.unsubscribe();
        }
      });
  }

  onEditPassenger() {
    this.isEditing = true;
  }
}
