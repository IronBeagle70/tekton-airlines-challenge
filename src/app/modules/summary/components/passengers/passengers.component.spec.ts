import { DebugElement } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';

import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { Passenger } from '@shared/models/passenger/passenger.model';

import { PassengerComponent } from '../passenger/passenger.component';
import { PassengersComponent } from './passengers.component';

describe('PassengersComponent', () => {
  let component: PassengersComponent;
  let fixture: ComponentFixture<PassengersComponent>;
  let el: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MatSnackBarModule, MatIconModule, MatDialogModule],
      declarations: [PassengersComponent, PassengerComponent],
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(PassengersComponent);
        component = fixture.componentInstance;
        el = fixture.debugElement;
      });
  });

  it('passengers component should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should display a list of passengers', fakeAsync(() => {
    const passengers: Passenger[] = [
      {
        names: 'names test',
        surnames: 'surnames test',
        nationality: 'nationality test',
        documentType: 'DNI',
        documentNumber: '123',
      },
      {
        names: 'names test',
        surnames: 'surnames test',
        nationality: 'nationality test',
        documentType: 'CE',
        documentNumber: '1234',
      },
    ];
    component.passengers = passengers;

    fixture.detectChanges();

    fixture.whenStable().then(() => {
      const passengerElements =
        el.nativeElement.querySelectorAll('app-passenger');
      const passengersContainer = el.nativeElement.querySelector(
        '.passengers__container'
      );

      expect(passengersContainer.children.length).toBe(2);
      expect(passengerElements.length).toBe(2);
      expect(Array.from(passengersContainer.children)).toEqual(
        Array.from(passengerElements)
      );
    });
  }));
});
