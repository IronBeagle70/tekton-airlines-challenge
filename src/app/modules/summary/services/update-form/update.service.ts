import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UpdateService {
  private isEditingSubject = new BehaviorSubject<boolean>(false);

  isEditing$ = this.isEditingSubject.asObservable();

  setIsEditing(isEditing: boolean) {
    this.isEditingSubject.next(isEditing);
  }
}
