import { TestBed } from '@angular/core/testing';

import { UpdateService } from './update.service';

describe('UpdateService', () => {
  let service: UpdateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UpdateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should set isEditing in true correctly', () => {
    service.setIsEditing(true);
    service.isEditing$.subscribe((value) => {
      expect(value).toBe(true);
    });
  });

  it('should set isEditing in false correctly', () => {
    service.setIsEditing(false);
    service.isEditing$.subscribe((value) => {
      expect(value).toBe(false);
    });
  });
});
