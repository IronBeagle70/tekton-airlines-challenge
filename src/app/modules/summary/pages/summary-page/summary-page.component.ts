import { Component, OnInit } from '@angular/core';
import { Passenger } from '@shared/models/passenger/passenger.model';
import { StoreService } from '@shared/services/store/store.service';

@Component({
  selector: 'app-summary-page',
  templateUrl: './summary-page.component.html',
  styleUrls: ['./summary-page.component.scss'],
})
export class SummaryPageComponent implements OnInit {
  passengers: Passenger[] = [];

  constructor(private storeService: StoreService) {}

  ngOnInit(): void {
    this.storeService.passengers$.subscribe((passenger) => {
      this.passengers = passenger;
    });
  }
}
