import { NgModule } from '@angular/core';

import { SharedModule } from '@shared/shared.module';
import { RegisterRoutingModule } from '@modules/register/register-routing.module';

import { RegisterPageComponent } from '@modules/register/pages/register-page/register-page.component';
import { RegisterFormComponent } from '@modules/register/components/register-form/register-form.component';

@NgModule({
  declarations: [
    RegisterPageComponent,
    RegisterFormComponent
  ],
  imports: [
    SharedModule,
    RegisterRoutingModule,
  ]
})
export class RegisterModule { }
