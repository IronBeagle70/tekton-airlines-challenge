import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { By } from '@angular/platform-browser';

import { NotificationsService } from '@shared/services/notifications/notifications.service';
import { ValidatorsService } from '@shared/services/validators/validators.service';

import { SharedModule } from '@shared/shared.module';

import { FormErrorContainerComponent } from '@shared/components/form-extensions/form-error-container/form-error-container.component';
import { FormErrorMessageComponent } from '@shared/components/form-extensions/form-error-message/form-error-message.component';
import { RegisterFormComponent } from './register-form.component';

describe('RegisterFormComponent', () => {
  let component: RegisterFormComponent;
  let fixture: ComponentFixture<RegisterFormComponent>;
  let el: DebugElement;
  let validatorsService: ValidatorsService;
  let notifierService: NotificationsService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [
        RegisterFormComponent,
        FormErrorContainerComponent,
        FormErrorMessageComponent,
      ],
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(RegisterFormComponent);
        component = fixture.componentInstance;
        el = fixture.debugElement;
        validatorsService = TestBed.inject(ValidatorsService);
        notifierService = TestBed.inject(NotificationsService);
      });
  });

  it('register form should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should display register form', () => {
    const formContainer = el.nativeElement.querySelectorAll('.form');
    const formInputContainer = el.nativeElement.querySelectorAll(
      '.form__container div'
    );
    const formTitle = el.query(By.css('.form__title'));

    expect(formContainer).toBeTruthy();
    expect(formTitle.nativeElement.innerText).toBe('Registro de Pasajeros');
    expect(formInputContainer.length).toBe(6);
  });

  it('should validate names control', () => {
    const namesControl = component.namesControl;

    expect(namesControl.invalid).toBeTruthy();
    expect(namesControl.errors?.['required']).toBeTruthy();

    namesControl.setValue('123');
    expect(namesControl.invalid).toBeTruthy();
    expect(namesControl.errors?.['namesMustbeValid']).toBeTruthy();

    namesControl.setValue("names's námÉñ,NaMe");
    expect(namesControl.valid).toBeTruthy();
    expect(namesControl.errors).toBeNull();
  });

  it('should validate surnames control', () => {
    const surnamesControl = component.surnamesControl;

    expect(surnamesControl.invalid).toBeTruthy();
    expect(surnamesControl.errors?.['required']).toBeTruthy();

    surnamesControl.setValue('@');
    expect(surnamesControl.invalid).toBeTruthy();
    expect(surnamesControl.errors?.['surnamesMustbeValid']).toBeTruthy();

    surnamesControl.setValue(',SúRnameZ námÉñ,ÑnNaMe');
    expect(surnamesControl.valid).toBeTruthy();
    expect(surnamesControl.errors).toBeNull();
  });

  it('should validate nationality control', () => {
    const nationalityControl = component.nationalityControl;

    expect(nationalityControl.invalid).toBeTruthy();
    expect(nationalityControl.errors?.['required']).toBeTruthy();

    nationalityControl.setValue('@');
    expect(nationalityControl.invalid).toBeTruthy();
    expect(nationalityControl.errors?.['nationalityMustbeValid']).toBeTruthy();

    nationalityControl.setValue('Nátioó ñauú líty');
    expect(nationalityControl.valid).toBeTruthy();
    expect(nationalityControl.errors).toBeNull();
  });

  it('Should validate document type control', () => {
    const documentType = component.documentType;

    expect(documentType.length).toBe(3);

    const documentTypeControl = component.documentTypeControl;

    expect(documentTypeControl.invalid).toBeTruthy;
    expect(documentTypeControl.errors?.['required']).toBeTruthy();
  });

  it('should set custom validators for documentNumber with type DNI', () => {
    const documentNumberControl = component.documentNumberControl;

    expect(documentNumberControl.invalid).toBeTruthy();
    expect(documentNumberControl.errors?.['required']).toBeTruthy();

    validatorsService.setupDocumentNumberValidators(
      documentNumberControl,
      'DNI'
    );

    documentNumberControl.setValue('123abc789');
    expect(documentNumberControl.errors?.['dniMustBeValid']).toBeTruthy();
    expect(documentNumberControl.invalid).toBeTruthy();

    documentNumberControl.setValue('12345678');
    expect(documentNumberControl.valid).toBeTruthy();
    expect(documentNumberControl.errors).toBeNull();
  });

  it('should set custom validators for documentNumber with type CE', () => {
    const documentNumberControl = component.documentNumberControl;

    expect(documentNumberControl.invalid).toBeTruthy();
    expect(documentNumberControl.errors?.['required']).toBeTruthy();

    validatorsService.setupDocumentNumberValidators(
      documentNumberControl,
      'CE'
    );

    documentNumberControl.setValue('123');
    expect(documentNumberControl.errors?.['ceMustBeValid']).toBeTruthy();
    expect(documentNumberControl.invalid).toBeTruthy();

    documentNumberControl.setValue('123AbC789');
    expect(documentNumberControl.valid).toBeTruthy();
    expect(documentNumberControl.errors).toBeNull();
  });

  it('should set custom validators for documentNumber with type Passport', () => {
    const documentNumberControl = component.documentNumberControl;

    expect(documentNumberControl.invalid).toBeTruthy();
    expect(documentNumberControl.errors?.['required']).toBeTruthy();

    validatorsService.setupDocumentNumberValidators(
      documentNumberControl,
      'Pasaporte'
    );

    documentNumberControl.setValue('1234567890');
    expect(documentNumberControl.errors?.['passportMustBevalid']).toBeTruthy();
    expect(documentNumberControl.invalid).toBeTruthy();

    documentNumberControl.setValue('123456789');
    expect(documentNumberControl.valid).toBeTruthy();
    expect(documentNumberControl.errors).toBeNull();
  });

  it('should have a enabled submit button and create when the form is valid', () => {
    component.namesControl.setValue('Names test');
    component.surnamesControl.setValue('Surnames test');
    component.nationalityControl.setValue('Nationality test');
    component.documentTypeControl.setValue('DNI');
    component.documentNumberControl.setValue('12345678');

    fixture.detectChanges();

    const submitButton = fixture.debugElement.query(
      By.css('button[type="submit"]')
    ).nativeElement;

    const spy = spyOn(component, 'onCreatePassenger').and.callThrough();
    const notifierServiceSpy = spyOn(notifierService, 'showNotification');
    submitButton.click();

    expect(submitButton.disabled).toBeFalsy();
    expect(spy).toHaveBeenCalled();
    expect(notifierServiceSpy).toHaveBeenCalledWith(
      'Pasajero Registrado Satisfactoriamente',
      'Ok'
    );
  });

  it('should have a disabled submit button when the form is invalid', () => {
    component.namesControl.setValue('Names');
    component.surnamesControl.setValue('Surna4mes test');
    component.nationalityControl.setValue('Nationality @test');
    component.documentTypeControl.setValue('');
    component.documentNumberControl.setValue('12345678');

    fixture.detectChanges();

    const submitButton = fixture.debugElement.query(
      By.css('button[type="submit"]')
    ).nativeElement;

    expect(submitButton.disabled).toBeTruthy();
  });

  it('should active the cancel form button', () => {
    const buttons = el.nativeElement.querySelectorAll('button');
    const submitButton = buttons[0];
    const cancelButton = buttons[1];

    const spy = spyOn(component, 'onCancel').and.callThrough();
    const formSpy = spyOn(component.passengerForm, 'reset');
    cancelButton.click();

    fixture.detectChanges();

    expect(submitButton.disabled).toBeTruthy();
    expect(spy).toHaveBeenCalled();
    expect(formSpy).toHaveBeenCalled();
  });
});
