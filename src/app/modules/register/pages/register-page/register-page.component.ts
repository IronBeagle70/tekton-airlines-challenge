import { Component, OnInit } from '@angular/core';
import { StoreService } from '@shared/services/store/store.service';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.scss'],
})
export class RegisterPageComponent implements OnInit {
  passengersNumber = 0;

  constructor(private storeService: StoreService) {}

  ngOnInit(): void {
    this.storeService.passengers$.subscribe((passengers) => {
      this.passengersNumber = passengers.length;
    });
  }
}
