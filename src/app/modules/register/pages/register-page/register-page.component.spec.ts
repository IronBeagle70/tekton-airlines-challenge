import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { By } from '@angular/platform-browser';
import { of } from 'rxjs';

import { Passenger } from '@shared/models/passenger/passenger.model';
import { NumberMessagePipe } from '@shared/pipes/number-message/number-message.pipe';
import { StoreService } from '@shared/services/store/store.service';

import { FormErrorContainerComponent } from '@shared/components/form-extensions/form-error-container/form-error-container.component';
import { FormErrorMessageComponent } from '@shared/components/form-extensions/form-error-message/form-error-message.component';
import { RegisterFormComponent } from '@modules/register/components/register-form/register-form.component';
import { RegisterPageComponent } from './register-page.component';


describe('RegisterPageComponent', () => {
  let component: RegisterPageComponent;
  let fixture: ComponentFixture<RegisterPageComponent>;
  let storeServiceSpy: jasmine.SpyObj<StoreService>;
  let el: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MatSnackBarModule, ReactiveFormsModule, MatDialogModule],
      declarations: [
        RegisterPageComponent,
        RegisterFormComponent,
        NumberMessagePipe,
        FormErrorContainerComponent,
        FormErrorMessageComponent,
      ],
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(RegisterPageComponent);
        component = fixture.componentInstance;
        el = fixture.debugElement;
        storeServiceSpy = TestBed.inject(
          StoreService
        ) as jasmine.SpyObj<StoreService>;
      });
  });

  it('register component should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should display register form', () => {
    const registerForm = el.query(By.css('app-register-form'));
    expect(registerForm).toBeTruthy();
  });

  it('should display register alert correctly if there are no passengers', () => {
    storeServiceSpy.passengers$ = of([]);
    fixture.detectChanges();
    const registerAlert = el.query(By.css('.register__alert'));

    expect(registerAlert.nativeElement.innerText).toBe(
      'Aún no hay pasajeros registrados'
    );
  });

  it('should display register alert correctly if there are passengers', () => {
    const passengers: Passenger[] = [
      {
        names: 'test names 1',
        surnames: 'test surnames 1',
        nationality: 'test nationality 1',
        documentType: 'test documentType 1',
        documentNumber: 'test documentNumber 1',
      },
      {
        names: 'test names 2',
        surnames: 'test surnames 2',
        nationality: 'test nationality 2',
        documentType: 'test documentType 2',
        documentNumber: 'test documentNumber 2',
      },
    ];
    storeServiceSpy.passengers$ = of(passengers);
    fixture.detectChanges();

    const registerAlert = el.query(By.css('.register__alert'));

    expect(registerAlert.nativeElement.innerText).toEqual('Pasajeros 2 de 4');
  });

  it('should display register progress correctly if there are no passengers', () => {
    storeServiceSpy.passengers$ = of([]);
    fixture.detectChanges();

    const registerProgress: HTMLElement = el.query(
      By.css('.register__progress progress')
    ).nativeElement;

    expect(registerProgress.getAttribute('max')).toBe('4');
    expect(registerProgress.getAttribute('value')).toBe('0');
  });

  it('should display register progress correctly if there are passengers', () => {
    const passengers: Passenger[] = [
      {
        names: 'test names 1',
        surnames: 'test surnames 1',
        nationality: 'test nationality 1',
        documentType: 'test documentType 1',
        documentNumber: 'test documentNumber 1',
      },
    ];
    storeServiceSpy.passengers$ = of(passengers);
    fixture.detectChanges();

    const registerProgress: HTMLElement = el.query(
      By.css('.register__progress progress')
    ).nativeElement;

    expect(registerProgress.getAttribute('max')).toBe('4');
    expect(registerProgress.getAttribute('value')).toBe('1');
  });

  it('should display verify btn correctly if there are no passengers', () => {
    storeServiceSpy.passengers$ = of([]);
    fixture.detectChanges();
    const verifyBtn = el.query(By.css('.verify__btn a'));
    expect(verifyBtn).toBeNull();
  });

  it('should display verify btn correctly if there are no passengers', () => {
    const passengers: Passenger[] = [
      {
        names: 'test names 1',
        surnames: 'test surnames 1',
        nationality: 'test nationality 1',
        documentType: 'test documentType 1',
        documentNumber: 'test documentNumber 1',
      },
    ];
    storeServiceSpy.passengers$ = of(passengers);
    fixture.detectChanges();

    const verifyBtn = el.query(By.css('.verify__btn a'));

    expect(verifyBtn.nativeElement.innerText).toBe('Verificar Datos');
  });
});
